import java.util.concurrent.ThreadLocalRandom;

  
  /* si k est grand, on aura tendance à changer de machine en machine sinon on reste jouer sur une même machine  
   * plus le  k est petit, plus l'UCB se rapprochera de la gloutonne
   * K est le paramètre pour régler le compromis exploration/exploitation
   * 
   * gloutonne est meilleure si l'esperance est elevée
   * UCB est meilleure si la variance est elevée
   * */


public class UCB {

	public static void main(String[] args) {
		
		for (int i=0;i<100;i++){
			System.out.print(" Essai "+i );
			eval(Math.sqrt(2));
		}
	}
	
	static void eval(double k){
		// Initialisation des manchots
		Manchot[] listemanchot = getListeManchots(15);
		/*for (int i=0; i<10; i++){
			System.out.println( " Manchot   " + (i+1) +
								" Esperance " + listemanchot[i].getEsperance() +
								" Variance  " + listemanchot[i].getVariance());
		}*/
		
		// Stratégie aléatoire
		System.out.println( " Somme aléatoire : "+ rechercheAleatoire(10,listemanchot));
		
		// Recherche gloutonne
		System.out.println(" Somme gloutonne " + rechercheGloutonne(100, listemanchot));

		// UCB
		System.out.println(" Somme UCB " + rechercheUCB(100, listemanchot, k)+ " k = "+ k);
		
		System.out.println("___________________________________________");
	}

	static int getRandom(int min, int max){
		return ThreadLocalRandom.current().nextInt(min, max + 1);
	}
	
	static Manchot[] getListeManchots(int nombre){
		Manchot[] liste = new Manchot[nombre];
		for (int i=0;i<nombre;i++){
			liste[i] = new Manchot(getRandom(-nombre,nombre),getRandom(0, nombre));
		}
		return liste;
	}

	static double rechercheAleatoire(int nbIterations, Manchot[] liste){
		Double somme = (double) 0;
		for (int i=0;i<nbIterations;i++){
			int nummanchot = getRandom(0,liste.length-1);
			somme += liste[nummanchot].tirerBras();
		}
		return somme;
	}
	
	/*
	 * Dans cette fonction le choix du bras se fera comme suit : 
	 * 1. Essayer chaque bras une fois et retenir le meilleur bras. 
	 * 2. Pour toutes les itérations restantes choisir le bras retenu.
	 * */
	static double rechercheGloutonne(int nbIterations, Manchot[] listemanchot){
		int max = 0;
		double tirageencours = 0,tiragemax = 0, somme = 0;
		
		for (int i = 0; i<listemanchot.length-1;i++){
			tirageencours = listemanchot[i].tirerBras();
			if (tirageencours>tiragemax) max=i;
		}
		
		System.out.println(" Machine choisie "+max);
		
		for (int i=0; i<(nbIterations-listemanchot.length);i++){
			somme += listemanchot[max].tirerBras();
		}
		return somme;
	}
	
	/*
	 * Dans cette fonction le choix du bras se fera comme suit :
		1. Créer deux tableaux pour stocker le nombre de tirages de chaque bras et la somme des scores de chaque bras.
		2. Essayer chaque bras une fois et mettre à jour les deux tableaux précédents.
		3. Pour toutes les itérations restantes choisir le bras en suivant la formule UCB vue en cours, en remplaçant la
			constante d’exploration ( 2 dans le cours) par une variable K.
		La fonction doit là encore retourner la somme des gains cumulés.
*/
	static double rechercheUCB(int nbIterations, Manchot[] listemanchot, double k){
		int[] tirage_bras = new int[listemanchot.length];
		double[] score_bras = new double[listemanchot.length];
		double somme = 0;
		double curUCB = 0, bestUCB =0;
		int braschoisi = 0;
		
		// On commence par parcourir une fois chaque manchot
		for (int i = 0; i<listemanchot.length-1;i++){
			tirage_bras[i] = tirage_bras[i] + 1;
			score_bras[i] = score_bras[i] + listemanchot[i].tirerBras() ;
		}
		
		//  On parcourt tous les manchots une fois pour calculer le meilleur via l'UCB
		for (int i=0; i<listemanchot.length-1;i++){
				curUCB = kUCB(score_bras[i],nbIterations,tirage_bras[i],k);
				if (curUCB>bestUCB) braschoisi = i;
		}
		
		
		// On fait le reste des essais en commançant par braschoisi
		for (int i=0; i<(nbIterations-listemanchot.length);i++){
			// On ajoute 1 au tirage du bras en cours
			tirage_bras[braschoisi] = tirage_bras[braschoisi] + 1;
			// On tire sur le bras choisi et on somme avec le tableau
			score_bras[braschoisi] = score_bras[braschoisi] + listemanchot[braschoisi].tirerBras() ;
			
			// On recalcule tous les UCB et on prend le meilleur, on réaffecte le braschoisi
			for (int j=0;j<listemanchot.length-1;j++){
				curUCB = kUCB(score_bras[j],i,tirage_bras[j],2);
				if (curUCB>bestUCB) braschoisi = j ;
			}
		}
		
		nombre_parbras(tirage_bras);
		return somme_totale(score_bras);
	}

	static void nombre_parbras(int[] bras){
		for (int i=0;i<bras.length-1;i++){
			System.out.print(" "+bras[i]);
		}
		System.out.println("");
	}
	
	static double somme_totale(double[] score_bras){
		double somme = 0;
		for (int i=0;i<score_bras.length-1;i++){
			somme += score_bras[i];
		}
		return somme;
	}
	
	static double kUCB(double gain, int n, int ni, double k){
		return gain + k*Math.sqrt(Math.log(n)/ni);
	}
}
