import java.util.*;
import java.math.*;

public class Manchot {
		private double esperance;
		private double variance;
		
		public Manchot(double esperance, double variance) {
			super();
			this.esperance = esperance;
			this.variance = variance;
		}
		
		public double getEsperance() {
			return esperance;
		}

		public void setEsperance(double esperance) {
			this.esperance = esperance;
		}

		public double getVariance() {
			return variance;
		}

		public void setVariance(double variance) {
			this.variance = variance;
		}
		
		double tirerBras() {
			Gaussian g = new Gaussian();
			return g.getGaussian(esperance, variance);
			};
		
		
}
